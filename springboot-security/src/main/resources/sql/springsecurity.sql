/*
 Navicat Premium Data Transfer

 Source Server         : database
 Source Server Type    : MySQL
 Source Server Version : 50713
 Source Host           : localhost:3306
 Source Schema         : springsecurity

 Target Server Type    : MySQL
 Target Server Version : 50713
 File Encoding         : 65001

 Date: 24/07/2019 15:11:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for pers
-- ----------------------------
DROP TABLE IF EXISTS `pers`;
CREATE TABLE `pers`  (
  `per_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '权限id',
  `per_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '权限名称',
  `per_url` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '权限对应的请求',
  `per_status` tinyint(2) NOT NULL COMMENT '这个权限是否有效0无效1有效',
  PRIMARY KEY (`per_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for persistent_logins
-- ----------------------------
DROP TABLE IF EXISTS `persistent_logins`;
CREATE TABLE `persistent_logins`  (
  `username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `series` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `last_used` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`series`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of persistent_logins
-- ----------------------------
INSERT INTO `persistent_logins` VALUES ('admin', '/LjQpH115sgwXdottk5DNQ==', 'BytwDdh8AA9ghbOFRKResw==', '2019-07-19 16:11:39');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '权限id',
  `role_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '权限名称',
  `role_per` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色对应的权限',
  `role_status` tinyint(2) NOT NULL DEFAULT 1 COMMENT '权限是否被删除0已删除1未删除',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES (1, '超级管理员', '1@2', 1);
INSERT INTO `roles` VALUES (2, '超级管理员', '1@2', 1);
INSERT INTO `roles` VALUES (3, '超级管理员', '1@2', 1);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户表主键',
  `username` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登录名',
  `password` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `enabled` tinyint(1) NOT NULL DEFAULT 1 COMMENT '用于登录时账户是否有效0无效1有效',
  `account_non_expired` tinyint(1) NOT NULL DEFAULT 1 COMMENT '账号是否过期0已过期1未过期',
  `credentials_non_expire` tinyint(1) NOT NULL DEFAULT 1 COMMENT '密码是否过期0已过期1未过期',
  `account_non_locked` tinyint(1) NOT NULL DEFAULT 1 COMMENT '账户是否被锁定0已锁定1未锁定',
  `user_role` bigint(10) NOT NULL COMMENT '权限id集合用@符号隔开',
  `status` tinyint(2) NOT NULL DEFAULT 1 COMMENT '整个账户是否有效0无效1有效',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 1, 1, 1, 1, 1, 1);
INSERT INTO `users` VALUES (2, 'asd', '7815696ecbf1c96e6894b779456d330e', 1, 1, 1, 1, 2, 1);
INSERT INTO `users` VALUES (3, 'admin1', '7815696ecbf1c96e6894b779456d330e', 1, 1, 1, 1, 3, 1);

SET FOREIGN_KEY_CHECKS = 1;
