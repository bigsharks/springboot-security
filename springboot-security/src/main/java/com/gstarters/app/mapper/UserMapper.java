package com.gstarters.app.mapper;

import org.apache.ibatis.annotations.Param;

import com.gstarters.app.model.User;
/**
 * 
 * @author gg
 *
 * 2019年7月17日 下午9:36:33
 */
public interface UserMapper {
	
    int deleteByPrimaryKey(Long userId);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Long userId);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);
    
    User findByUserName(@Param("userName")String userName);
}