package com.gstarters.app.mapper;

import com.gstarters.app.model.Per;

public interface PerMapper {
    int deleteByPrimaryKey(Long perId);

    int insert(Per record);

    int insertSelective(Per record);

    Per selectByPrimaryKey(Long perId);

    int updateByPrimaryKeySelective(Per record);

    int updateByPrimaryKey(Per record);
}