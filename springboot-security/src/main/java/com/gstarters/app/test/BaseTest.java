package com.gstarters.app.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.gstarters.app.SpringBootSecurityApp;
import com.gstarters.app.config.CommonConfig;

/**
 * 
 * @author gg
 *
 * 2019年7月16日 下午3:19:40
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes=SpringBootSecurityApp.class)
public class BaseTest {
	
	@Autowired
	private CommonConfig config;

	@Test
	public void test(){
		System.out.println(config.getLogin().getLoginPage());
	}
}
