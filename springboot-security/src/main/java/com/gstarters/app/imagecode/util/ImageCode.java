package com.gstarters.app.imagecode.util;

import java.awt.image.BufferedImage;
import java.time.LocalDateTime;

import lombok.Data;

/**
 * 图片验证码参数
 * @author gg
 *
 * 2019年7月19日 下午12:51:59
 */
@Data
public class ImageCode {
	
	private BufferedImage image;
	
	private String code;
	
	private LocalDateTime expireTime;
	//expireTime过期时间单位:秒
	public ImageCode(BufferedImage image,String code,int expireTime){
		this.image = image;
		this.code = code;
		this.expireTime = LocalDateTime.now().plusSeconds(expireTime);
	}
	
	/**
	 * 判断是否过期
	 * @return
	 */
	public boolean isExpired(){
		return LocalDateTime.now().isAfter(expireTime);
	}

}
