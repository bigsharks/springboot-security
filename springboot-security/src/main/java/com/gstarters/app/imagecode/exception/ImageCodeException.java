package com.gstarters.app.imagecode.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * 自定义图片验证码异常
 * @author gg
 *
 * 2019年7月19日 下午1:35:03
 */
public class ImageCodeException extends AuthenticationException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5642912151473760448L;

	public ImageCodeException(String msg) {
		super(msg);
	}

}
