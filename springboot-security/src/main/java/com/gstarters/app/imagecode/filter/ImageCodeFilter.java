package com.gstarters.app.imagecode.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.social.connect.web.HttpSessionSessionStrategy;
import org.springframework.social.connect.web.SessionStrategy;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.filter.OncePerRequestFilter;

import com.gstarters.app.authentication.MyAuthenticationFailureHandler;
import com.gstarters.app.imagecode.controller.ImageCodeController;
import com.gstarters.app.imagecode.exception.ImageCodeException;
import com.gstarters.app.imagecode.util.ImageCode;

import lombok.extern.slf4j.Slf4j;

/**
 * 图片验证码登录时的过滤器
 * 
 * @author gg
 *
 *         2019年7月19日 下午1:27:52
 */
@Slf4j
public class ImageCodeFilter extends OncePerRequestFilter {

	private SessionStrategy sessionStrategy = new HttpSessionSessionStrategy();
	@Autowired
	private AuthenticationFailureHandler myAuthenticationFailureHandler;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		String uri = request.getRequestURI();
		log.info("[ImageCodeFilter] -> uri -> {}", uri);
		String method = request.getMethod();
		log.info("[ImageCodeFilter] -> method -> {}", method);
		if (StringUtils.equals("/authentication/form", uri) && StringUtils.endsWithIgnoreCase(method, "post")) {
			try {
				validate(new ServletWebRequest(request));
			} catch (ImageCodeException e) {
				log.info("[ImageCodeFilter] -> ImageCodeException -> {}", e);
				throw new RuntimeException(e);
				//myAuthenticationFailureHandler.onAuthenticationFailure(request, response, e);
			}
		}
		filterChain.doFilter(request, response);

	}

	@SuppressWarnings("unused")
	private void validate(ServletWebRequest request) throws ServletRequestBindingException {
		ImageCode codeInSession = (ImageCode) sessionStrategy.getAttribute(request, ImageCodeController.SESSION_KEY);
		log.info("[ImageCodeFilter] -> codeInSession -> {}", codeInSession);
		String code = codeInSession.getCode();
		log.info("[ImageCodeFilter] -> code -> {}", code);
		String codeInRequest = ServletRequestUtils.getStringParameter(request.getRequest(), "imageCode");
		log.info("[ImageCodeFilter] -> codeInRequest -> {}", codeInRequest);
		if (codeInRequest.equals(code)) {
			sessionStrategy.removeAttribute(request, ImageCodeController.SESSION_KEY);
		} else {
			throw new RuntimeException("验证码不正确");
		}
	}

	public SessionStrategy getSessionStrategy() {
		return sessionStrategy;
	}

	public void setSessionStrategy(SessionStrategy sessionStrategy) {
		this.sessionStrategy = sessionStrategy;
	}

	public AuthenticationFailureHandler getMyAuthenticationFailureHandler() {
		return myAuthenticationFailureHandler;
	}

	public void setMyAuthenticationFailureHandler(AuthenticationFailureHandler myAuthenticationFailureHandler) {
		this.myAuthenticationFailureHandler = myAuthenticationFailureHandler;
	}


}
