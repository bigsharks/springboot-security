package com.gstarters.app.model;

import lombok.Data;

/**
 * 用户角色
 * @author gg
 *
 * 2019年7月17日 下午9:59:12
 */
@Data
public class Role {
    private Long roleId;

    private String roleName;

    private String rolePer;

    private Byte roleStatus;

}