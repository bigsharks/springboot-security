package com.gstarters.app.model;

import lombok.Data;
/**
 * 用户账户
 * @author gg
 *
 * 2019年7月17日 下午9:58:57
 */
@Data
public class User {
    private Long userId;

    private String username;

    private String password;

    private Boolean enabled;

    private Boolean accountNonExpired;

    private Boolean credentialsNonExpire;

    private Boolean accountNonLocked;

    private Long userRole;

    private Byte status;

}