package com.gstarters.app.model;

import lombok.Data;
/**
 * 用户权限
 * @author gg
 *
 * 2019年7月17日 下午9:59:45
 */
@Data
public class Per {
	
    private Long perId;

    private String perName;

    private String perUrl;

    private Byte perStatus;

}