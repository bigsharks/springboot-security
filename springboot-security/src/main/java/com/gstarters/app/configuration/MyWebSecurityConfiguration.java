package com.gstarters.app.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import com.gstarters.app.config.CommonConfig;
import com.gstarters.app.imagecode.filter.ImageCodeFilter;

/**
 * WebSecurityConfigurerAdapter springsecurity适配器类
 * 
 * @author gg
 *
 *         2019年7月16日 下午3:50:21
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class MyWebSecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private CommonConfig commonConfig;
	
	@Autowired
	private AuthenticationSuccessHandler myAuthenticationSuccessHandler;
	@Autowired
	private AuthenticationFailureHandler myAuthenticationFailureHandler;
	@Autowired
	private LogoutSuccessHandler myLogoutSuccessHandler;
	@Autowired
	private DataSource dataSource;
	@Autowired
	private UserDetailsService userDetailsService;
	/**
	 * 实现记住我
	 * @return
	 */
	@Bean
	public PersistentTokenRepository persistentTokenRepository(){
		JdbcTokenRepositoryImpl jdbcTokenRepositoryImpl = new JdbcTokenRepositoryImpl();
		jdbcTokenRepositoryImpl.setDataSource(dataSource);
		//启动时创建表
		jdbcTokenRepositoryImpl.setCreateTableOnStartup(false);
		return jdbcTokenRepositoryImpl;
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		/**
		 * 默认
		 * http.formLogin()
		 * .and()
		 * .authorizeRequests()
		 * .anyRequest()
		 * .authenticated()
		 */
		/**
		 * 手动跳转页面
		 * // 表单登录
			http.formLogin()
				// 浏览器弹出框验证
				// http.httpBasic()
				//自定义登录页面
				.loginPage("/login_in.html")
				//自定义登录时请求
				.loginProcessingUrl("/authentication/form")
				.and()
				// 从下面匹配请求
				.authorizeRequests()
				//所有人都能访问
				.antMatchers("/login_in.html").permitAll()
				// 允许一些请求
				.anyRequest()
				// 任何请求都需要身份验证
				.authenticated()
				.and()
				//在默认情况下springsecurity启动跨站访问保护 
				//注意在配置使用了自定义登录和登陆页面时关闭
				.csrf().disable();
		 */
		/**
		 * http.formLogin()
				// 浏览器弹出框验证
				// http.httpBasic()
				//自定义登录请求 可以是请求 也可以是页面
				.loginPage("/authentication/require")
				//自定义登录时请求 设置form表单登录请求
				.loginProcessingUrl("/authentication/form")
				.and()
				// 从下面匹配请求
				.authorizeRequests()
				//所有人都能访问
				.antMatchers("/authentication/require",commonConfig.getLogin().getLoginPage()).permitAll()
				// 允许一些请求
				.anyRequest()
				// 任何请求都需要身份验证
				.authenticated()
				.and()
				//在默认情况下springsecurity启动跨站访问保护 
				//注意在配置使用了自定义登录和登陆页面时关闭
				.csrf().disable();
		 */
		/**
		 * // 表单登录
		http.formLogin()
				// 浏览器弹出框验证
				// http.httpBasic()
				//自定义登录请求 可以是请求 也可以是页面
				.loginPage("/authentication/require")
				//自定义登录时请求 设置form表单登录请求
				.loginProcessingUrl("/authentication/form")
				//自定义登录成功处理器
				.successHandler(myAuthenticationSuccessHandler)
				//自定义登录失败处理器
				.failureHandler(myAuthenticationFailureHandler)
				.and()
				//自定义退出
				.logout()
				.logoutUrl("/my/logout")
				.logoutSuccessHandler(myLogoutSuccessHandler)
				//退出成功的请求
				//.logoutSuccessUrl("/login_in.html")
				//删除缓存
				.deleteCookies("JSESSIONID")
				.and()
				// 从下面匹配请求
				.authorizeRequests()
				//所有人都能访问
				.antMatchers("/code/image","/user/register","/login_register.html","/js/jquery.js","/login_failure.html","/authentication/require",commonConfig.getLogin().getLoginPage()).permitAll()
				// 允许一些请求
				.anyRequest()
				// 任何请求都需要身份验证
				.authenticated()
				.and()
				//在默认情况下springsecurity启动跨站访问保护 
				//注意在配置使用了自定义登录和登陆页面时关闭
				.csrf().disable();
		 */
		/**
		 * 	// 表单登录
				http.formLogin()
						// 浏览器弹出框验证
						// http.httpBasic()
						//自定义登录请求 可以是请求 也可以是页面
						.loginPage("/authentication/require")
						//自定义登录时请求 设置form表单登录请求
						.loginProcessingUrl("/authentication/form")
						//自定义登录成功处理器
						.successHandler(myAuthenticationSuccessHandler)
						//自定义登录失败处理器
						.failureHandler(myAuthenticationFailureHandler)
						.and()
						//自定义退出
						.logout()
						.logoutUrl("/my/logout")
						.logoutSuccessHandler(myLogoutSuccessHandler)
						//退出成功的请求
						//.logoutSuccessUrl("/login_in.html")
						//删除缓存
						.deleteCookies("JSESSIONID")
						.and()
						// 从下面匹配请求
						.authorizeRequests()
						//所有人都能访问
						.antMatchers("/code/image","/user/register","/login_register.html","/js/jquery.js","/login_failure.html","/authentication/require",commonConfig.getLogin().getLoginPage()).permitAll()
						// 允许一些请求
						.anyRequest()
						// 任何请求都需要身份验证
						.authenticated()
						.and()
						//在默认情况下springsecurity启动跨站访问保护 
						//注意在配置使用了自定义登录和登陆页面时关闭
						.csrf().disable();
		 */
		ImageCodeFilter imageCodeFilter = new ImageCodeFilter();
		imageCodeFilter.setMyAuthenticationFailureHandler(myAuthenticationFailureHandler);
		http.addFilterBefore(imageCodeFilter, UsernamePasswordAuthenticationFilter.class);
				// 表单登录
					http
						//图片验证码处理器
						.formLogin()
						// 浏览器弹出框验证
						// http.httpBasic()
						//自定义登录请求 可以是请求 也可以是页面
						.loginPage("/authentication/require")
						//自定义登录时请求 设置form表单登录请求
						.loginProcessingUrl("/authentication/form")
						//自定义登录成功处理器
						.successHandler(myAuthenticationSuccessHandler)
						//自定义登录失败处理器
						.failureHandler(myAuthenticationFailureHandler)
						.and()
						.rememberMe()
						.tokenRepository(persistentTokenRepository())
						//记住我有效时间
						.tokenValiditySeconds(commonConfig.getLogin().getRememberMeSeconds())
						.userDetailsService(userDetailsService)
						.and()
						//自定义退出
						.logout()
						.logoutUrl("/my/logout")
						.logoutSuccessHandler(myLogoutSuccessHandler)
						//退出成功的请求
						//.logoutSuccessUrl("/login_in.html")
						//删除缓存
						.deleteCookies("JSESSIONID")
						.and()
						// 从下面匹配请求
						.authorizeRequests()
						//所有人都能访问
						.antMatchers("/code/image","/user/register","/login_register.html","/js/jquery.js","/login_failure.html","/authentication/require",commonConfig.getLogin().getLoginPage()).permitAll()
						// 允许一些请求
						.anyRequest()
						// 任何请求都需要身份验证
						.authenticated()
						.and()
						//在默认情况下springsecurity启动跨站访问保护 
						//注意在配置使用了自定义登录和登陆页面时关闭
						.csrf().disable();
					
	}
}
