package com.gstarters.app.configuration;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.gstarters.app.util.BaseUtil;
/**
 * 自定义加密
 * @author gg
 *
 * 2019年7月17日 下午11:06:38
 */
@Component
public class MyPasswordEncoder implements PasswordEncoder{

	@Override
	public String encode(CharSequence rawPassword) {
		return BaseUtil.md5((String) rawPassword);
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {
		return encodedPassword.equals(BaseUtil.md5((String) rawPassword));
	}

}
