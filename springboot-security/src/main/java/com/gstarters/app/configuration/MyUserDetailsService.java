package com.gstarters.app.configuration;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.gstarters.app.mapper.RoleMapper;
import com.gstarters.app.mapper.UserMapper;
import com.gstarters.app.model.Role;
import com.gstarters.app.util.BaseUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author gg
 *
 *         2019年7月16日 下午4:49:19
 */
@Component
@Slf4j
public class MyUserDetailsService implements UserDetailsService {

	@Autowired
	private UserMapper userMapper;
	@Autowired
	private RoleMapper roleMapper;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		log.info("{}", "用户名为->" + username);
		com.gstarters.app.model.User findByUserName = userMapper.findByUserName(username);
		Long userRole = findByUserName.getUserRole();
		Role role = roleMapper.selectByPrimaryKey(userRole);
		List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
		GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(role.getRoleName());
		grantedAuthorities.add(grantedAuthority);
		String password = findByUserName.getPassword();
		log.info("[MyUserDetailsService] -> 登录密码  -> {}",password);
		boolean enabled = findByUserName.getEnabled();
		log.info("[MyUserDetailsService] -> 是否可用  -> {}",enabled);
		boolean accountNonExpired = findByUserName.getAccountNonExpired();
		log.info("[MyUserDetailsService] -> 账户是否过期  -> {}",accountNonExpired);
		boolean credentialsNonExpire = findByUserName.getCredentialsNonExpire();
		log.info("[MyUserDetailsService] -> 密码是否过期  -> {}",credentialsNonExpire);
		boolean accountNonLocked = findByUserName.getAccountNonLocked();
		log.info("[MyUserDetailsService] -> 账户是否被锁定  -> {}",accountNonLocked);
		//
		// 根据用户名查找信息 用户名 密码 权限集合
		// return new User(username, new
		// BCryptPasswordEncoder().encode("1234567"),
		// AuthorityUtils.commaSeparatedStringToAuthorityList("admin"));
		/**
		 * username: 
		 * password:
		 *  enabled: false不可用
		 *   accountNonExpired: false账户过期
		 * credentialsNonExpired: false密码过期 
		 * accountNonLocked: false代表账户被锁定
		 * authorities: 角色集合
		 */
		return new User(username, password, enabled, accountNonExpired, credentialsNonExpire, accountNonLocked,
				grantedAuthorities);
	}

}
