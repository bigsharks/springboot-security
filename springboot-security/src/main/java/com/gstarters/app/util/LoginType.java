package com.gstarters.app.util;
/**
 * 登录类型
 * @author gg
 *
 * 2019年7月17日 下午4:11:42
 */
public enum LoginType {

		REDIRECT,
		JSON
}
