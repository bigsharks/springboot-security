package com.gstarters.app.util;

import lombok.Data;

/**
 * 响应信息
 * @author gg
 *
 * 2019年7月16日 下午8:26:18
 */
@Data
public class SimpleResponse {
	public SimpleResponse(Object content){
		this.content = content;
	}
	private Object content;


}
