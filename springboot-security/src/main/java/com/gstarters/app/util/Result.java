package com.gstarters.app.util;

import lombok.Data;

/**
 * 自定义数据
 * @author gg
 *
 * 2019年7月17日 下午9:55:33
 */
@Data
public class Result {
	
	private int code;
	
	private String message;
	
	private Object data = "";
	
	public Result(ResultCode resultCode){
		this.code = resultCode.getCode();
		this.message = resultCode.getMessage();
	}
	
	public Result(ResultCode resultCode,Object data){
		this.code = resultCode.getCode();
		this.message = resultCode.getMessage();
		this.data = data;
	}

}
