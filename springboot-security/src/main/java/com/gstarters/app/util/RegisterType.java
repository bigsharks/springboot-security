package com.gstarters.app.util;
/**
 * 注册类型
 * @author gg
 *
 * 2019年7月17日 下午8:45:27
 */
public enum RegisterType {
	SUPERADMIN,
	ADMIN,
	USER
}
