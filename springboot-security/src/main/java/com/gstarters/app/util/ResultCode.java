package com.gstarters.app.util;


/**
 * 
 * @author gg
 *
 * 2019年7月17日 下午9:50:59
 */
public enum ResultCode {
	
	SYS_FAILURE(0,"系统处理失败!"),
	SYS_SUCCESS(1,"系统处理成功!"),
	REGISTER_SUCCESS(2,"注册成功!"),
	REGISTER_FAILURE(3,"注册失败!"),
	USERNAME_IS_EXIST(4,"用户名已存在!");
	
	private int code;
	private String message;
	
	ResultCode(int code,String message){
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
