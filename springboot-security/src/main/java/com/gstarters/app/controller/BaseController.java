package com.gstarters.app.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.gstarters.app.config.CommonConfig;
import com.gstarters.app.util.SimpleResponse;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author gg
 *
 *         2019年7月16日 下午7:33:53
 */
@Slf4j
@RestController
public class BaseController {

	@Autowired
	private CommonConfig commonConfig;

	private RequestCache requestCache = new HttpSessionRequestCache();
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

	/**
	 * 当需身份认证是跳转到这
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 * @throws ServletException
	 */
	@RequestMapping("/authentication/require")
	@ResponseStatus(code = HttpStatus.UNAUTHORIZED)
	public Object requireAuthentication(HttpServletRequest request, HttpServletResponse response,String imageCode)
			throws IOException, ServletException {
		SavedRequest savedRequest = requestCache.getRequest(request, response);
		if (savedRequest != null) {
			String target = savedRequest.getRedirectUrl();
			log.info("引发跳转的请求-> ,{}", target);
			// 如果未认证访问时的请求的末尾是以.html结尾则访问登录页面 否则 返回未认证消息
			if (StringUtils.endsWithIgnoreCase(target, ".html")) {
				redirectStrategy.sendRedirect(request, response, commonConfig.getLogin().getLoginPage());
			}
		}
		
		//redirectStrategy.sendRedirect(request, response, commonConfig.getLogin().getLoginPage());
		return new SimpleResponse("访问的服务需要身份认证,请引导用户到登录页");
	}

}
