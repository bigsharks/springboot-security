package com.gstarters.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gstarters.app.config.CommonConfig;
import com.gstarters.app.config.PersConfig;
import com.gstarters.app.mapper.RoleMapper;
import com.gstarters.app.mapper.UserMapper;
import com.gstarters.app.model.Role;
import com.gstarters.app.model.User;
import com.gstarters.app.service.UserService;
import com.gstarters.app.util.BaseUtil;
import com.gstarters.app.util.RegisterType;
import com.gstarters.app.util.Result;
import com.gstarters.app.util.ResultCode;

import lombok.extern.slf4j.Slf4j;
/**
 * 
 * @author gg
 *
 * 2019年7月17日 下午8:57:20
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService{
	
	@Autowired
	private CommonConfig commonConfig;
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private RoleMapper roleMapper;

	@Transactional
	@Override
	public Object register(User user) {
		try {
			
			if(userMapper.findByUserName(user.getUsername()) != null){
				return new Result(ResultCode.USERNAME_IS_EXIST);
			}
			
			System.err.println(RegisterType.SUPERADMIN.equals(commonConfig.getRegister().getRegisterType()));
			Role role = new Role();
			if(RegisterType.SUPERADMIN.equals(commonConfig.getRegister().getRegisterType())){
				role.setRoleName(PersConfig.SUPER_ADMIN_ROLE_PERS_NAME);
				role.setRolePer(PersConfig.SUPER_ADMIN_ROLE_PERS);
			}else if(RegisterType.ADMIN.equals(commonConfig.getRegister().getRegisterType())){
				role.setRoleName(PersConfig.ADMIN_ROLE_PERS_NAME);
				role.setRolePer(PersConfig.ADMIN_ROLE_PERS);
			}else if(RegisterType.USER.equals(commonConfig.getRegister().getRegisterType())){
				role.setRolePer(PersConfig.USER_ROLE_PERS);
				role.setRoleName(PersConfig.USER_ROLE_PERS_NAME);
			}else{
				role.setRoleName("");
				role.setRolePer("");
			}
			role.setRoleStatus((byte) 1);
			log.info("[role] -> {}",role);
			int addRole = roleMapper.insertSelective(role);
			
			user.setEnabled(true);
			user.setPassword(BaseUtil.md5(user.getPassword()));
			user.setAccountNonExpired(true);
			user.setCredentialsNonExpire(true);
			user.setAccountNonExpired(true);
			user.setAccountNonLocked(true);
			user.setStatus((byte) 1);
			user.setUserRole(role.getRoleId());
			log.info("[user] -> {}",user);
			int addUser = userMapper.insertSelective(user);
			if(addRole > 0 && addUser > 0){
				return new Result(ResultCode.REGISTER_SUCCESS);
			}else{
				return new Result(ResultCode.REGISTER_FAILURE);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}