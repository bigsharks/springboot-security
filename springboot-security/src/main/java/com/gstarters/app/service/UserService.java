package com.gstarters.app.service;

import com.gstarters.app.model.User;

/**
 * 
 * @author gg
 *
 *         2019年7月17日 下午8:55:03
 */
public interface UserService {

	/**
	 * 注册
	 * 
	 * @param user
	 * @return
	 */
	Object register(User user);

}
