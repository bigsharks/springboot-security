package com.gstarters.app.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;
/**
 * 配置文件
 * @author gg
 *
 * 2019年7月17日 下午8:48:55
 */
@Component
@ConfigurationProperties(prefix="customer.base")
@Data
public class CommonConfig {
	
	private LoginProperties login = new LoginProperties();
	
	private RegsiterProperties register = new RegsiterProperties();

}
