package com.gstarters.app.config;

import com.gstarters.app.util.RegisterType;

import lombok.Data;

/**
 * 注册
 * @author gg
 *
 * 2019年7月17日 下午8:47:18
 */
@Data
public class RegsiterProperties {
	
	/**
	 * 注册类型
	 */
	private RegisterType registerType = RegisterType.USER;

}
