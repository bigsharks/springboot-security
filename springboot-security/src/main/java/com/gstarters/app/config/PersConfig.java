package com.gstarters.app.config;
/**
 * 基础权限配置
 * @author gg
 *
 * 2019年7月17日 下午8:29:12
 */
public class PersConfig {
	
	/**
	 * 普通用户
	 */
	public static final String USER_ROLE_PERS = "1@2";
	/**
	 * 普通用户
	 */
	public static final String USER_ROLE_PERS_NAME = "普通用户";
	/**
	 * 管理员
	 */
	public static final String ADMIN_ROLE_PERS = "1@2";
	/**
	 * 管理员
	 */
	public static final String ADMIN_ROLE_PERS_NAME = "管理员";
	/**
	 * 超级管理员拥有的权限
	 */
	public static final String SUPER_ADMIN_ROLE_PERS = "1@2";
	/**
	 * 超级管理员拥有的权限
	 */
	public static final String SUPER_ADMIN_ROLE_PERS_NAME = "超级管理员";
	
	public static final String[] EXCLUDE_LOGIN_URL = new String[]{
			//登录失败跳转的页面
			"login_failure.html",
			//登录时所用的页面
			"login_in.html",
			//注册是所用的页面
			"login_register.html",
			//处理登录页面时的请求
			"/authentication/require",
			//springsecurity提交时的请求
			"/authentication/form",
			//springsecurity退出请求
			"/my/logout",
			//静态资源文件
			"/jquery/jquery.js",
			//注册请求
			"/user/register"
	};
	
	public static final String[] INCLUDE_PERS_URL = new String[]{
			//登录成功是的页面
			"login_success.html",
			//主页
			"success.html"
	};
}
