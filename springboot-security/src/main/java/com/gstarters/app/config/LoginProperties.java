package com.gstarters.app.config;


import com.gstarters.app.util.LoginType;

import lombok.Data;
/**
 * 登录属性
 * @author gg
 *
 * 2019年7月17日 下午7:43:43
 */
@Data
public class LoginProperties {
	
	/**
	 * 默认登录页面
	 */
	private String loginPage = "/login_in.html";
	/**
	 * 默认数据形式(如果配置文件中为空 则使用当前配置)
	 */
	private LoginType loginType = LoginType.JSON;
	/**
	 * 记住我秒
	 */
	private int rememberMeSeconds = 3600;
}
