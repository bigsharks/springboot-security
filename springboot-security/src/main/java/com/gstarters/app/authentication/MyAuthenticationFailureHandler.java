package com.gstarters.app.authentication;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gstarters.app.config.CommonConfig;
import com.gstarters.app.util.LoginType;

/**
 * 自定义登录失败
 * 
 * @author gg
 *
 *         2019年7月17日 下午3:48:17
 */
/*
 * @Component("myAuthenticationFailureHandler") public class
 * MyAuthenticationFailureHandler implements AuthenticationFailureHandler{
 * 
 * private Logger log = LoggerFactory.getLogger(getClass());
 * 
 * @Autowired private ObjectMapper objectMapper;
 * 
 * @Override public void onAuthenticationFailure(HttpServletRequest request,
 * HttpServletResponse response, AuthenticationException authentication) throws
 * IOException, ServletException { log.info("登录失败!");
 * response.setContentType("application/json;charset=UTF-8");
 * response.getWriter().write(objectMapper.writeValueAsString(authentication));
 * }
 * 
 * }
 */
@Component("myAuthenticationFailureHandler")
public class MyAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private ObjectMapper objectMapper;
	@Autowired
	private CommonConfig commonConfig;

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authentication) throws IOException, ServletException {
		log.info("登录失败!");
		if (LoginType.JSON.equals(commonConfig.getLogin().getLoginType())) {
			response.setContentType("application/json;charset=UTF-8");
			response.getWriter().write(objectMapper.writeValueAsString(authentication));
		} else {
			response.sendRedirect("/login_failure.html");
		}

	}

}
