package com.gstarters.app.authentication;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gstarters.app.config.CommonConfig;
import com.gstarters.app.util.LoginType;

/**
 * 自定义退出
 * 
 * @author gg
 *
 *         2019年7月17日 下午5:08:17
 */
@Component("myLogoutSuccessHandler")
public class MyLogoutSuccessHandler implements LogoutSuccessHandler {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private CommonConfig commonConfig;
	@Autowired
	private ObjectMapper objectMapper;

	@Override
	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		log.info("退出成功!");
		if (LoginType.JSON.equals(commonConfig.getLogin().getLoginType())) {
			response.setContentType("application/json;charset=UTF-8");
			response.getWriter().write(objectMapper.writeValueAsString(authentication));
		} else {
			response.sendRedirect("/login_in.html");
		}

	}

}
