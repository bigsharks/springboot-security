package com.gstarters.app;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 * springboot启动
 * @author gg
 *
 * 2019年7月17日 下午8:54:38
 */
@EnableTransactionManagement
@SpringBootApplication
@RestController
@MapperScan(basePackages={"com.gstarters.app.mapper"})
public class SpringBootSecurityApp {

	public static void main(String[] args) {
		
		SpringApplication.run(SpringBootSecurityApp.class, args);
		
	}
	
	@GetMapping("/hello")
	String hello(){
		return "hello springboot security";
	}
	
}
